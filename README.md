## References

- https://github.com/bxcodec/go-clean-arch
- https://github.com/gofiber/recipes/tree/master/clean-architecture/
- https://github.com/ThreeDotsLabs/monolith-microservice-shop
- https://github.com/ThreeDotsLabs/go-web-app-antipatterns
- https://dave.cheney.net/2019/01/08/avoid-package-names-like-base-util-or-common
- https://talks.cuonglm.xyz/escape-analysis-in-go-compiler.slide
- https://slides.com/jalex-chang/go-esc
- https://softwareengineering.stackexchange.com/questions/123627/what-are-the-london-and-chicago-schools-of-tdd
