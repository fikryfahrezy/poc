package main

import (
	"fmt"
	"gitlab.com/fikryfahrezy/poc/article"
	"gitlab.com/fikryfahrezy/poc/author"
	"os"
	"sync"
	"time"

	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"gitlab.com/fikryfahrezy/poc/book"
	"log"
)

func mysqlConnection() string {
	mysqlDbHost := os.Getenv("POC_MYSQLDB_HOST")
	if mysqlDbHost == "" {
		mysqlDbHost = "localhost"
	}

	mysqlDbPort := os.Getenv("POC_MYSQLDB_PORT")
	if mysqlDbPort == "" {
		mysqlDbPort = "3306"
	}

	mysqlDbUser := os.Getenv("POC_MYSQLDB_USER")
	if mysqlDbUser == "" {
		mysqlDbUser = "root"
	}
	mysqlDbPass := os.Getenv("POC_MYSQLDB_PASS")
	if mysqlDbPass == "" {
		mysqlDbPass = "root"
	}

	mysqlDbName := os.Getenv("POC_MYSQLDB_NAME")
	if mysqlDbName == "" {
		mysqlDbName = "article"
	}

	return fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", mysqlDbUser, mysqlDbPass, mysqlDbHost, mysqlDbPort, mysqlDbName)
}

func main() {
	mysqlDb, err := sql.Open("mysql", mysqlConnection())
	if err != nil {
		log.Fatal(err)
	}
	err = mysqlDb.Ping()
	if err != nil {
		log.Fatal(err)
	}

	defer func() {
		err := mysqlDb.Close()
		if err != nil {
			log.Fatal(err)
		}
	}()

	var bookCollection = new(sync.Map)
	bookRepo := book.NewRepository(bookCollection)
	bookService := book.NewUsecase(bookRepo)

	authorRepo := author.NewRepository(mysqlDb)
	ar := article.NewRepository(mysqlDb)
	timeoutContext := 2 * time.Second
	au := article.NewUsecase(ar, authorRepo, timeoutContext)

	app := fiber.New()
	app.Use(cors.New())
	app.Get("/", func(ctx *fiber.Ctx) error {
		return ctx.Send([]byte("Welcome to the clean-architecture mongo book shop!"))
	})

	api := app.Group("/api")
	book.Router(api, bookService)
	article.Router(api, au)

	log.Fatal(app.Listen(":8080"))
}
