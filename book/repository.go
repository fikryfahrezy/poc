package book

import (
	"fmt"
	"sync"
	"time"
)

//Repository interface allows us to access the CRUD Operations in mongo here.
type Repository interface {
	CreateBook(book BookEntity) (BookEntity, error)
	ReadBook() ([]BookView, error)
	UpdateBook(book BookEntity) (BookEntity, error)
	DeleteBook(ID string) error
}

type repository struct {
	Collection *sync.Map
}

//NewRepository is the single instance repo that is being created.
func NewRepository(collection *sync.Map) Repository {
	return &repository{
		Collection: collection,
	}
}

//CreateBook is a mongo repository that helps to create books
func (r *repository) CreateBook(book BookEntity) (BookEntity, error) {
	book.ID = fmt.Sprintf("%d", time.Now().Unix())
	book.CreatedAt = time.Now()
	book.UpdatedAt = time.Now()
	r.Collection.Store(book.ID, book)
	return book, nil
}

//ReadBook is a mongo repository that helps to fetch books
func (r *repository) ReadBook() ([]BookView, error) {
	var books []BookView
	r.Collection.Range(func(key, value interface{}) bool {
		fmt.Println(key, value)
		return true
	})
	return books, nil
}

//UpdateBook is a mongo repository that helps to update books
func (r *repository) UpdateBook(book BookEntity) (BookEntity, error) {
	book.UpdatedAt = time.Now()
	r.Collection.Store(book.ID, book)
	return book, nil
}

//DeleteBook is a mongo repository that helps to delete books
func (r *repository) DeleteBook(ID string) error {
	r.Collection.Delete(ID)
	return nil
}
