package book

// Usecase is an interface from which our api module can access our repository of all our models
type Usecase interface {
	InsertBook(book PlaceBookCommand) (Book, error)
	FetchBooks() ([]BookView, error)
	UpdateBook(book PlaceBookCommand) (Book, error)
	RemoveBook(ID string) error
}

type usecase struct {
	repository Repository
}

//NewUsecase is used to create a single instance of the service
func NewUsecase(r Repository) Usecase {
	return &usecase{
		repository: r,
	}
}

//InsertBook is a service layer that helps insert book in BookShop
func (s *usecase) InsertBook(book PlaceBookCommand) (Book, error) {
	newBook, err := s.repository.CreateBook(MapPlaceBookCmdToEntity(book))
	if err != nil {
		return Book{}, err
	}

	bookResult := MapBookEntityToBook(newBook)
	return bookResult, nil
}

//FetchBooks is a service layer that helps fetch all books in BookShop
func (s *usecase) FetchBooks() ([]BookView, error) {
	return s.repository.ReadBook()
}

//UpdateBook is a service layer that helps update books in BookShop
func (s *usecase) UpdateBook(book PlaceBookCommand) (Book, error) {
	updatedBook, err := s.repository.UpdateBook(MapPlaceBookCmdToEntity(book))
	if err != nil {
		return Book{}, err
	}

	bookCmd := MapBookEntityToBook(updatedBook)
	return bookCmd, nil
}

//RemoveBook is a service layer that helps remove books from BookShop
func (s *usecase) RemoveBook(ID string) error {
	return s.repository.DeleteBook(ID)
}
