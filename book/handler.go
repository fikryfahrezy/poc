package book

import (
	"github.com/gofiber/fiber/v2"
	"github.com/pkg/errors"
	"net/http"
)

// AddBook is handler/controller which creates Books in the BookShop
func AddBook(service Usecase) fiber.Handler {
	return func(c *fiber.Ctx) error {
		var requestBody PlaceBookRequest
		err := c.BodyParser(&requestBody)
		if err != nil {
			c.Status(http.StatusBadRequest)
			return c.JSON(BookErrorResponse(err))
		}
		if requestBody.Author == "" || requestBody.Title == "" {
			c.Status(http.StatusInternalServerError)
			return c.JSON(BookErrorResponse(errors.New(
				"Please specify title and author")))
		}
		result, err := service.InsertBook(MapPlaceBookReqToCmd(requestBody))
		if err != nil {
			c.Status(http.StatusInternalServerError)
			return c.JSON(BookErrorResponse(err))
		}
		return c.JSON(BookSuccessResponse(MapBookToRes(result)))
	}
}

// UpdateBook is handler/controller which updates data of Books in the BookShop
func UpdateBook(service Usecase) fiber.Handler {
	return func(c *fiber.Ctx) error {
		var requestBody PlaceBookRequest
		err := c.BodyParser(&requestBody)
		if err != nil {
			c.Status(http.StatusBadRequest)
			return c.JSON(BookErrorResponse(err))
		}
		result, err := service.UpdateBook(MapPlaceBookReqToCmd(requestBody))
		if err != nil {
			c.Status(http.StatusInternalServerError)
			return c.JSON(BookErrorResponse(err))
		}
		return c.JSON(BookSuccessResponse(MapBookToRes(result)))
	}
}

// RemoveBook is handler/controller which removes Books from the BookShop
func RemoveBook(service Usecase) fiber.Handler {
	return func(c *fiber.Ctx) error {
		var requestBody DeleteRequest
		err := c.BodyParser(&requestBody)
		if err != nil {
			c.Status(http.StatusBadRequest)
			return c.JSON(BookErrorResponse(err))
		}
		bookID := requestBody.ID
		err = service.RemoveBook(bookID)
		if err != nil {
			c.Status(http.StatusInternalServerError)
			return c.JSON(BookErrorResponse(err))
		}
		return c.JSON(BookMessageResponse("updated successfully"))
	}
}

// GetBooks is handler/controller which lists all Books from the BookShop
func GetBooks(service Usecase) fiber.Handler {
	return func(c *fiber.Ctx) error {
		fetched, err := service.FetchBooks()
		if err != nil {
			c.Status(http.StatusInternalServerError)
			return c.JSON(BookErrorResponse(err))
		}
		return c.JSON(BooksSuccessResponse(fetched))
	}
}
