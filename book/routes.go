package book

import (
	"github.com/gofiber/fiber/v2"
)

// Router is the Router for GoFiber App
func Router(app fiber.Router, service Usecase) {
	app.Get("/books", GetBooks(service))
	app.Post("/books", AddBook(service))
	app.Put("/books", UpdateBook(service))
	app.Delete("/books", RemoveBook(service))
}
