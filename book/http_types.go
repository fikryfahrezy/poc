package book

// DeleteRequest struct is used to parse Delete Reqeusts for Books
type DeleteRequest struct {
	ID string `json:"id"`
}

// BookView is the presenter object which will be passed in the response by Handler
type BookView struct {
	ID     string `json:"id"`
	Title  string `json:"title"`
	Author string `json:"author"`
}

type PlaceBookRequest struct {
	ID     string `json:"id"`
	Title  string `json:"title"`
	Author string `json:"author"`
}
