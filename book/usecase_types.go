package book

type PlaceBookCommand struct {
	Title  string
	Author string
}

type Book struct {
	ID     string
	Title  string
	Author string
}
