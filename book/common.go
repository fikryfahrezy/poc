package book

import (
	"github.com/gofiber/fiber/v2"
)

// BookSuccessResponse is the singular SuccessResponse that will be passed in the response by Handler
func BookSuccessResponse(bookRes BookView) *fiber.Map {
	return &fiber.Map{
		"status": true,
		"data":   bookRes,
		"error":  nil,
	}
}

// BooksSuccessResponse is the list SuccessResponse that will be passed in the response by Handler
func BooksSuccessResponse(data []BookView) *fiber.Map {
	return &fiber.Map{
		"status": true,
		"data":   data,
		"error":  nil,
	}
}

// BookErrorResponse is the ErrorResponse that will be passed in the response by Handler
func BookErrorResponse(err error) *fiber.Map {
	return &fiber.Map{
		"status": false,
		"data":   "",
		"error":  err.Error(),
	}
}

func BookMessageResponse(msg string) *fiber.Map {
	return &fiber.Map{
		"status": true,
		"data":   msg,
		"error":  nil,
	}
}

func MapPlaceBookReqToCmd(book PlaceBookRequest) PlaceBookCommand {
	return PlaceBookCommand{
		Title:  book.Title,
		Author: book.Author,
	}
}

func MapBookToRes(book Book) BookView {
	return BookView{
		ID:     book.ID,
		Title:  book.Title,
		Author: book.Author,
	}
}

func MapPlaceBookCmdToEntity(book PlaceBookCommand) BookEntity {
	return BookEntity{
		Title:  book.Title,
		Author: book.Author,
	}
}

func MapBookEntityToBook(book BookEntity) Book {
	return Book{
		ID:     book.ID,
		Title:  book.Title,
		Author: book.Author,
	}
}
