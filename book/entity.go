package book

import (
	"time"
)

// BookEntity Constructs your Book model under entities.
type BookEntity struct {
	ID        string
	Title     string
	Author    string
	CreatedAt time.Time
	UpdatedAt time.Time
}
