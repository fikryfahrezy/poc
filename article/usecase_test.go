package article_test

import (
	"context"
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/fikryfahrezy/poc/article"
	articleMock "gitlab.com/fikryfahrezy/poc/article/mocks"
	"gitlab.com/fikryfahrezy/poc/author"
	authorMock "gitlab.com/fikryfahrezy/poc/author/mocks"
)

func TestUsecaseFetch(t *testing.T) {
	mockArticleRepo := new(articleMock.Repository)
	mockArticle := article.ArticleEntity{
		Title:   "Hello",
		Content: "Content",
	}

	mockListArtilce := make([]article.ArticleEntity, 0)
	mockListArtilce = append(mockListArtilce, mockArticle)

	t.Run("success", func(t *testing.T) {
		mockArticleRepo.On("Fetch", mock.Anything, mock.AnythingOfType("string"),
			mock.AnythingOfType("int64")).Return(mockListArtilce, "next-cursor", nil).Once()
		mockAuthor := author.Author{
			ID:   1,
			Name: "Iman Tumorang",
		}
		mockAuthorrepo := new(authorMock.Repository)
		mockAuthorrepo.On("GetByID", mock.Anything, mock.AnythingOfType("int64")).Return(mockAuthor, nil)
		u := article.NewUsecase(mockArticleRepo, mockAuthorrepo, time.Second*2)
		num := int64(1)
		cursor := "12"
		list, nextCursor, err := u.Fetch(context.TODO(), cursor, num)
		cursorExpected := "next-cursor"
		assert.Equal(t, cursorExpected, nextCursor)
		assert.NotEmpty(t, nextCursor)
		assert.NoError(t, err)
		assert.Len(t, list, len(mockListArtilce))

		mockArticleRepo.AssertExpectations(t)
		mockAuthorrepo.AssertExpectations(t)
	})

	t.Run("error-failed", func(t *testing.T) {
		mockArticleRepo.On("Fetch", mock.Anything, mock.AnythingOfType("string"),
			mock.AnythingOfType("int64")).Return(nil, "", errors.New("Unexpexted Error")).Once()

		mockAuthorrepo := new(authorMock.Repository)
		u := article.NewUsecase(mockArticleRepo, mockAuthorrepo, time.Second*2)
		num := int64(1)
		cursor := "12"
		list, nextCursor, err := u.Fetch(context.TODO(), cursor, num)

		assert.Empty(t, nextCursor)
		assert.Error(t, err)
		assert.Len(t, list, 0)
		mockArticleRepo.AssertExpectations(t)
		mockAuthorrepo.AssertExpectations(t)
	})

}

func TestUsecaseGetByID(t *testing.T) {
	mockArticleRepo := new(articleMock.Repository)
	mockArticle := article.ArticleEntity{
		Title:   "Hello",
		Content: "Content",
	}
	mockAuthor := author.Author{
		ID:   1,
		Name: "Iman Tumorang",
	}

	t.Run("success", func(t *testing.T) {
		mockArticleRepo.On("GetByID", mock.Anything, mock.AnythingOfType("int64")).Return(mockArticle, nil).Once()
		mockAuthorrepo := new(authorMock.Repository)
		mockAuthorrepo.On("GetByID", mock.Anything, mock.AnythingOfType("int64")).Return(mockAuthor, nil)
		u := article.NewUsecase(mockArticleRepo, mockAuthorrepo, time.Second*2)

		a, err := u.GetByID(context.TODO(), mockArticle.ID)

		assert.NoError(t, err)
		assert.NotNil(t, a)

		mockArticleRepo.AssertExpectations(t)
		mockAuthorrepo.AssertExpectations(t)
	})
	t.Run("error-failed", func(t *testing.T) {
		mockArticleRepo.On("GetByID", mock.Anything, mock.AnythingOfType("int64")).Return(article.ArticleEntity{}, errors.New("Unexpected")).Once()

		mockAuthorrepo := new(authorMock.Repository)
		u := article.NewUsecase(mockArticleRepo, mockAuthorrepo, time.Second*2)

		a, err := u.GetByID(context.TODO(), mockArticle.ID)

		assert.Error(t, err)
		assert.Equal(t, article.Article{}, a)

		mockArticleRepo.AssertExpectations(t)
		mockAuthorrepo.AssertExpectations(t)
	})

}

func TestUsecaseStore(t *testing.T) {
	mockArticleRepo := new(articleMock.Repository)
	mockArticle := article.StoreArticleCommand{
		Title:   "Hello",
		Content: "Content",
	}

	t.Run("success", func(t *testing.T) {
		tempMockArticle := mockArticle
		mockArticleRepo.On("GetByTitle", mock.Anything, mock.AnythingOfType("string")).Return(article.ArticleEntity{}, article.ErrNotFound).Once()
		mockArticleRepo.On("Store", mock.Anything, mock.AnythingOfType("article.ArticleEntity")).Return(article.ArticleEntity{}, nil).Once()

		mockAuthorrepo := new(authorMock.Repository)
		u := article.NewUsecase(mockArticleRepo, mockAuthorrepo, time.Second*2)

		_, err := u.Store(context.TODO(), tempMockArticle)

		assert.NoError(t, err)
		assert.Equal(t, mockArticle.Title, tempMockArticle.Title)
		mockArticleRepo.AssertExpectations(t)
	})
	t.Run("existing-title", func(t *testing.T) {
		mockArticleRepo.On("GetByTitle", mock.Anything, mock.AnythingOfType("string")).Return(article.ArticleEntity{}, nil).Once()
		mockAuthor := author.Author{
			ID:   1,
			Name: "Iman Tumorang",
		}
		mockAuthorrepo := new(authorMock.Repository)
		mockAuthorrepo.On("GetByID", mock.Anything, mock.AnythingOfType("int64")).Return(mockAuthor, nil)

		u := article.NewUsecase(mockArticleRepo, mockAuthorrepo, time.Second*2)

		_, err := u.Store(context.TODO(), mockArticle)

		assert.Error(t, err)
		mockArticleRepo.AssertExpectations(t)
		mockAuthorrepo.AssertExpectations(t)
	})

}

func TestUsecaseDelete(t *testing.T) {
	mockArticleRepo := new(articleMock.Repository)
	mockArticle := article.ArticleEntity{
		Title:   "Hello",
		Content: "Content",
	}

	t.Run("success", func(t *testing.T) {
		mockArticleRepo.On("GetByID", mock.Anything, mock.AnythingOfType("int64")).Return(mockArticle, nil).Once()

		mockArticleRepo.On("Delete", mock.Anything, mock.AnythingOfType("int64")).Return(nil).Once()

		mockAuthorrepo := new(authorMock.Repository)
		u := article.NewUsecase(mockArticleRepo, mockAuthorrepo, time.Second*2)

		err := u.Delete(context.TODO(), mockArticle.ID)

		assert.NoError(t, err)
		mockArticleRepo.AssertExpectations(t)
		mockAuthorrepo.AssertExpectations(t)
	})
	t.Run("article-is-not-exist", func(t *testing.T) {
		mockArticleRepo.On("GetByID", mock.Anything, mock.AnythingOfType("int64")).Return(article.ArticleEntity{}, nil).Once()

		mockAuthorrepo := new(authorMock.Repository)
		u := article.NewUsecase(mockArticleRepo, mockAuthorrepo, time.Second*2)

		err := u.Delete(context.TODO(), mockArticle.ID)

		assert.Error(t, err)
		mockArticleRepo.AssertExpectations(t)
		mockAuthorrepo.AssertExpectations(t)
	})
	t.Run("error-happens-in-db", func(t *testing.T) {
		mockArticleRepo.On("GetByID", mock.Anything, mock.AnythingOfType("int64")).Return(article.ArticleEntity{}, errors.New("Unexpected Error")).Once()

		mockAuthorrepo := new(authorMock.Repository)
		u := article.NewUsecase(mockArticleRepo, mockAuthorrepo, time.Second*2)

		err := u.Delete(context.TODO(), mockArticle.ID)

		assert.Error(t, err)
		mockArticleRepo.AssertExpectations(t)
		mockAuthorrepo.AssertExpectations(t)
	})

}

func TestUsecaseUpdate(t *testing.T) {
	mockArticleRepo := new(articleMock.Repository)
	mockArticle := article.ArticleEntity{
		Title:   "Hello",
		Content: "Content",
	}
	mockArticleCommand := article.StoreArticleCommand{
		Title:   "Hello",
		Content: "Content",
	}

	t.Run("success", func(t *testing.T) {
		mockArticleRepo.On("Update", mock.Anything, mockArticle).Once().Return(mockArticle, nil)

		mockAuthorrepo := new(authorMock.Repository)
		u := article.NewUsecase(mockArticleRepo, mockAuthorrepo, time.Second*2)

		err := u.Update(context.TODO(), mockArticleCommand)
		assert.NoError(t, err)
		mockArticleRepo.AssertExpectations(t)
	})
}
