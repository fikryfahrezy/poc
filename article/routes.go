package article

import "github.com/gofiber/fiber/v2"

// Router is the Router for GoFiber App
func Router(app fiber.Router, service Usecase) {
	app.Get("/articles", FetchArticle(service))
	app.Post("/articles", Store(service))
	app.Get("/articles/:id", GetByID(service))
	app.Delete("/articles/:id", Delete(service))
}
