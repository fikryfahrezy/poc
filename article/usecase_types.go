package article

import (
	"time"
)

type StoreArticleCommand struct {
	Title   string
	Content string
}

type Author struct {
	ID        int64
	Name      string
	CreatedAt string
	UpdatedAt string
}

type Article struct {
	ID        int64
	Title     string
	Content   string
	Author    Author
	UpdatedAt time.Time
	CreatedAt time.Time
}
