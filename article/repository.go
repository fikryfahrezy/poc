package article

import (
	"context"
	"database/sql"
	"fmt"
	"time"
)

// Repository represent the article's repository contract
type Repository interface {
	Fetch(ctx context.Context, cursor string, num int64) (articles []ArticleEntity, nextCursor string, err error)
	GetByID(ctx context.Context, id int64) (article ArticleEntity, err error)
	GetByTitle(ctx context.Context, title string) (article ArticleEntity, err error)
	Update(ctx context.Context, ar ArticleEntity) (article ArticleEntity, err error)
	Store(ctx context.Context, a ArticleEntity) (article ArticleEntity, err error)
	Delete(ctx context.Context, id int64) (err error)
}

type repository struct {
	MysqlDB *sql.DB
}

// NewRepository will create an object that represent the article.Repository interface
func NewRepository(mysqlDb *sql.DB) Repository {
	return &repository{MysqlDB: mysqlDb}
}

func (r *repository) fetch(ctx context.Context, query string, args ...interface{}) (result []ArticleEntity, err error) {
	rows, err := r.MysqlDB.QueryContext(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	defer func() {
		errRow := rows.Close()
		if errRow != nil {
			fmt.Println(errRow)
		}
	}()

	for rows.Next() {
		t := ArticleEntity{}
		authorID := int64(0)
		err = rows.Scan(
			&t.ID,
			&t.Title,
			&t.Content,
			&authorID,
			&t.UpdatedAt,
			&t.CreatedAt,
		)

		if err != nil {
			return nil, err
		}
		t.Author = AuthorEntity{
			ID: authorID,
		}
		result = append(result, t)
	}

	return result, nil
}

func (r *repository) Fetch(ctx context.Context, cursor string, num int64) (articles []ArticleEntity, nextCursor string, err error) {
	query := `
		SELECT
			id,
			title,
			content,
			author_id,
			updated_at,
			created_at
		FROM
			article
		WHERE
		created_at > ? 
		ORDER BY created_at 
		LIMIT ?
	`

	decodedCursor, err := DecodeCursor(cursor)
	if err != nil && cursor != "" {
		return nil, "", ErrBadParamInput
	}

	articles, err = r.fetch(ctx, query, decodedCursor, num)
	if err != nil {
		return nil, "", err
	}

	if len(articles) == int(num) {
		nextCursor = EncodeCursor(articles[len(articles)-1].CreatedAt)
	}

	return
}

func (r *repository) GetByID(ctx context.Context, id int64) (article ArticleEntity, err error) {
	query := `
		SELECT
			id,
			title,
			content,
			author_id,
			updated_at,
			created_at
		FROM
			article
		WHERE id = ?
	`

	list, err := r.fetch(ctx, query, id)
	if err != nil {
		return ArticleEntity{}, err
	}

	if len(list) > 0 {
		article = list[0]
	} else {
		return article, ErrNotFound
	}

	return
}

func (r *repository) GetByTitle(ctx context.Context, title string) (article ArticleEntity, err error) {
	query := `
		SELECT
			id,
			title,
			content,
			author_id,
			updated_at,
			created_at
		FROM
			article
		WHERE
			title = ?`

	list, err := r.fetch(ctx, query, title)
	if err != nil {
		return
	}

	if len(list) > 0 {
		article = list[0]
	} else {
		return article, ErrNotFound
	}

	return
}

func (r *repository) Update(ctx context.Context, ar ArticleEntity) (article ArticleEntity, err error) {
	ar.UpdatedAt = time.Now()
	query := `UPDATE article SET title = ?, content = ?, author_id = ?, updated_at = ? WHERE id = ?`

	stmt, err := r.MysqlDB.PrepareContext(ctx, query)
	if err != nil {
		return
	}

	res, err := stmt.ExecContext(ctx, ar.Title, ar.Content, ar.Author.ID, ar.UpdatedAt, ar.ID)
	if err != nil {
		return
	}
	affect, err := res.RowsAffected()
	if err != nil {
		return
	}
	if affect != 1 {
		err = fmt.Errorf("Weird  Behavior. Total Affected: %d", affect)
		return
	}

	return
}

func (r *repository) Store(ctx context.Context, a ArticleEntity) (article ArticleEntity, err error) {
	query := `INSERT  article SET title = ?, content = ?, author_id = ?, updated_at = ?, created_at = ?`
	stmt, err := r.MysqlDB.PrepareContext(ctx, query)
	if err != nil {
		return
	}

	res, err := stmt.ExecContext(ctx, a.Title, a.Content, a.Author.ID, a.UpdatedAt, a.CreatedAt)
	if err != nil {
		return
	}

	lastID, err := res.LastInsertId()
	if err != nil {
		return
	}

	article.ID = lastID

	return
}

func (r *repository) Delete(ctx context.Context, id int64) (err error) {
	query := "DELETE FROM article WHERE id = ?"

	stmt, err := r.MysqlDB.PrepareContext(ctx, query)
	if err != nil {
		return
	}

	res, err := stmt.ExecContext(ctx, id)
	if err != nil {
		return
	}

	rowsAfected, err := res.RowsAffected()
	if err != nil {
		return
	}

	if rowsAfected != 1 {
		err = fmt.Errorf("Weird  Behavior. Total Affected: %d", rowsAfected)
		return
	}

	return
}
