package article

import (
	"context"
	"time"

	"gitlab.com/fikryfahrezy/poc/author"
	"golang.org/x/sync/errgroup"
)

// Usecase represent the article's usecases
type Usecase interface {
	Fetch(ctx context.Context, cursor string, num int64) (articles []Article, nexCursor string, err error)
	GetByID(ctx context.Context, id int64) (article Article, err error)
	Update(ctx context.Context, ar StoreArticleCommand) (err error)
	GetByTitle(ctx context.Context, title string) (article Article, err error)
	Store(context.Context, StoreArticleCommand) (article Article, err error)
	Delete(ctx context.Context, id int64) (err error)
}

type usecase struct {
	articleRepo    Repository
	authorRepo     author.Repository
	contextTimeout time.Duration
}

// NewUsecase will create new an articleUsecase object representation of domain.ArticleUsecase interface
func NewUsecase(a Repository, ar author.Repository, timeout time.Duration) Usecase {
	return &usecase{
		articleRepo:    a,
		authorRepo:     ar,
		contextTimeout: timeout,
	}
}

func (u usecase) Fetch(ctx context.Context, cursor string, num int64) (articles []Article, nextCursor string, err error) {
	if num == 0 {
		num = 10
	}

	timeoutCtx, cancel := context.WithTimeout(ctx, u.contextTimeout)
	defer cancel()

	articleData, nextCursor, err := u.articleRepo.Fetch(ctx, cursor, num)
	if err != nil {
		return []Article{}, "", err
	}

	// Get the authorData's id
	articles = make([]Article, len(articleData))
	mapAuthors := map[int64]AuthorEntity{}
	for i, article := range articleData {
		articles[i] = MapArticleEntityToArticle(article)
		mapAuthors[article.Author.ID] = AuthorEntity{}
	}

	g, groupCtx := errgroup.WithContext(timeoutCtx)

	// Using goroutine to fetch the authorData's detail
	chanAuthor := make(chan AuthorEntity)
	for authorID := range mapAuthors {
		tempAuthorId := authorID
		g.Go(func() error {
			authorData, err := u.authorRepo.GetByID(groupCtx, tempAuthorId)
			if err != nil {
				return err
			}
			chanAuthor <- AuthorEntity{
				ID:        authorData.ID,
				Name:      authorData.Name,
				CreatedAt: authorData.CreatedAt,
				UpdatedAt: authorData.UpdatedAt,
			}
			return nil
		})
	}

	go func() {
		err = g.Wait()
		if err != nil {
			return
		}
		close(chanAuthor)
	}()

	for authorData := range chanAuthor {
		if authorData != (AuthorEntity{}) {
			mapAuthors[authorData.ID] = authorData
		}
	}

	if err = g.Wait(); err != nil {
		return []Article{}, "", err
	}

	// merge the authorData's data
	for index, item := range articles {
		if a, ok := mapAuthors[item.Author.ID]; ok {
			articles[index].Author = MapAuthorEntityToAuthor(a)
		}
	}

	return articles, nextCursor, nil
}

func (u usecase) GetByID(ctx context.Context, id int64) (Article, error) {
	timeoutCtx, cancel := context.WithTimeout(ctx, u.contextTimeout)
	defer cancel()

	articleData, err := u.articleRepo.GetByID(timeoutCtx, id)
	if err != nil {
		return Article{}, err
	}

	authorData, err := u.authorRepo.GetByID(timeoutCtx, articleData.Author.ID)
	if err != nil {
		return Article{}, err
	}

	articleData.Author = AuthorEntity{
		ID:        authorData.ID,
		Name:      authorData.Name,
		CreatedAt: authorData.CreatedAt,
		UpdatedAt: authorData.UpdatedAt,
	}

	article := MapArticleEntityToArticle(articleData)
	return article, nil
}

func (u usecase) Update(ctx context.Context, ar StoreArticleCommand) (err error) {
	timeoutCtx, cancel := context.WithTimeout(ctx, u.contextTimeout)
	defer cancel()

	newArticle := MapStoreArticleCmdToEntity(ar)

	_, err = u.articleRepo.Update(timeoutCtx, newArticle)
	return err
}

func (u usecase) GetByTitle(ctx context.Context, title string) (article Article, err error) {
	timeoutCtx, cancel := context.WithTimeout(ctx, u.contextTimeout)
	defer cancel()

	articleData, err := u.articleRepo.GetByTitle(timeoutCtx, title)
	if err != nil {
		return Article{}, err
	}

	authorData, err := u.authorRepo.GetByID(ctx, articleData.Author.ID)
	if err != nil {
		return Article{}, err
	}

	articleData.Author = AuthorEntity{
		ID:        authorData.ID,
		Name:      authorData.Name,
		CreatedAt: authorData.CreatedAt,
		UpdatedAt: authorData.UpdatedAt,
	}
	article = MapArticleEntityToArticle(articleData)

	return article, nil
}

func (u usecase) Store(ctx context.Context, article StoreArticleCommand) (newArticle Article, err error) {
	if ok, err := IsRequestValid(article); !ok {
		return Article{}, err
	}

	timeoutCtx, cancel := context.WithTimeout(ctx, u.contextTimeout)
	defer cancel()

	existedArticle, _ := u.GetByTitle(timeoutCtx, article.Title)
	if existedArticle != (Article{}) {
		return Article{}, ErrConflict
	}

	newArticleData, err := u.articleRepo.Store(ctx, MapStoreArticleCmdToEntity(article))
	return MapArticleEntityToArticle(newArticleData), err
}

func (u usecase) Delete(ctx context.Context, id int64) (err error) {
	timeoutCtx, cancel := context.WithTimeout(ctx, u.contextTimeout)
	defer cancel()

	existedArticle, err := u.articleRepo.GetByID(timeoutCtx, id)
	if err != nil {
		return err
	}
	if existedArticle == (ArticleEntity{}) {
		return ErrNotFound
	}

	return u.articleRepo.Delete(ctx, id)
}
