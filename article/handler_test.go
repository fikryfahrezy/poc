package article_test

import (
	"encoding/json"
	"github.com/bxcodec/faker"
	"github.com/gofiber/fiber/v2"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/fikryfahrezy/poc/article"
	"gitlab.com/fikryfahrezy/poc/article/mocks"
	"net/http"
	"strconv"
	"strings"
	"testing"
	"time"
)

func TestHandlerFetch(t *testing.T) {
	var mockArticle article.Article
	err := faker.FakeData(&mockArticle)
	assert.NoError(t, err)
	mockUCase := new(mocks.Usecase)
	mockListArticle := make([]article.Article, 0)
	mockListArticle = append(mockListArticle, mockArticle)
	num := 1
	cursor := "2"
	mockUCase.On("Fetch", mock.Anything, cursor, int64(num)).Return(mockListArticle, "10", nil)

	e := fiber.New()
	article.Router(e, mockUCase)
	req, err := http.NewRequest("GET", "/articles?num=1&cursor="+cursor, strings.NewReader(""))
	assert.NoError(t, err)

	resp, err := e.Test(req)
	require.NoError(t, err)

	responseCursor := resp.Header.Get("X-Cursor")
	assert.Equal(t, "10", responseCursor)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	mockUCase.AssertExpectations(t)
}

func TestHandlerFetchError(t *testing.T) {
	mockUCase := new(mocks.Usecase)
	num := 1
	cursor := "2"
	mockUCase.On("Fetch", mock.Anything, cursor, int64(num)).Return(nil, "", article.ErrInternalServerError)

	e := fiber.New()
	article.Router(e, mockUCase)
	req, err := http.NewRequest("GET", "/articles?num=1&cursor="+cursor, strings.NewReader(""))
	assert.NoError(t, err)

	resp, err := e.Test(req)
	require.NoError(t, err)

	responseCursor := resp.Header.Get("X-Cursor")
	assert.Equal(t, "", responseCursor)
	assert.Equal(t, http.StatusInternalServerError, resp.StatusCode)
	mockUCase.AssertExpectations(t)
}

func TestHandlerGetByID(t *testing.T) {
	var mockArticle article.Article
	err := faker.FakeData(&mockArticle)
	assert.NoError(t, err)

	mockUCase := new(mocks.Usecase)

	num := int(mockArticle.ID)

	mockUCase.On("GetByID", mock.Anything, int64(num)).Return(mockArticle, nil)

	e := fiber.New()
	article.Router(e, mockUCase)
	req, err := http.NewRequest("GET", "/articles/"+strconv.Itoa(num), strings.NewReader(""))
	assert.NoError(t, err)

	resp, err := e.Test(req)
	require.NoError(t, err)

	assert.Equal(t, http.StatusOK, resp.StatusCode)
	mockUCase.AssertExpectations(t)
}

func TestHandlerStore(t *testing.T) {
	mockArticle := article.Article{
		Title:     "Title",
		Content:   "Content",
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}

	tempMockArticle := mockArticle
	tempMockArticle.ID = 0
	mockUCase := new(mocks.Usecase)

	j, err := json.Marshal(article.StoreArticleRequest{
		Title:   "Title",
		Content: "Content",
	})
	assert.NoError(t, err)

	mockUCase.On("Store", mock.Anything, mock.AnythingOfType("article.StoreArticleCommand")).Return(article.Article{}, nil)

	e := fiber.New()
	article.Router(e, mockUCase)
	req, err := http.NewRequest("POST", "/articles", strings.NewReader(string(j)))
	req.Header.Set("Content-Type", "application/json")
	assert.NoError(t, err)

	resp, err := e.Test(req)
	require.NoError(t, err)

	assert.Equal(t, http.StatusCreated, resp.StatusCode)
	mockUCase.AssertExpectations(t)
}

func TestHandlerDelete(t *testing.T) {
	var mockArticle article.Article
	err := faker.FakeData(&mockArticle)
	assert.NoError(t, err)

	mockUCase := new(mocks.Usecase)

	num := int(mockArticle.ID)

	mockUCase.On("Delete", mock.Anything, int64(num)).Return(nil)

	e := fiber.New()
	article.Router(e, mockUCase)
	req, err := http.NewRequest("DELETE", "/articles/"+strconv.Itoa(num), strings.NewReader(""))
	assert.NoError(t, err)

	resp, err := e.Test(req)
	require.NoError(t, err)

	assert.Equal(t, http.StatusAccepted, resp.StatusCode)
	mockUCase.AssertExpectations(t)

}
