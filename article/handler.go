package article

import (
	"context"
	"github.com/gofiber/fiber/v2"
	"net/http"
	"strconv"
)

func FetchArticle(service Usecase) fiber.Handler {
	return func(c *fiber.Ctx) error {
		num, _ := strconv.Atoi(c.Query("num"))
		cursor := c.Query("cursor")
		var ctx context.Context

		listAr, nextCursor, err := service.Fetch(ctx, cursor, int64(num))
		if err != nil {
			c.Status(GetStatusCode(err))
			return c.JSON(ArticleErrorResponse(err))
		}
		c.Response().Header.Set("X-Cursor", nextCursor)

		return c.JSON(ArticlesSuccessResponse(MapArticlesToRes(listAr)))
	}
}

func Store(service Usecase) fiber.Handler {
	return func(c *fiber.Ctx) error {
		var article StoreArticleRequest
		if err := c.BodyParser(&article); err != nil {
			c.Status(http.StatusUnprocessableEntity)
			return c.JSON(ArticleErrorResponse(err))
		}

		var ctx context.Context
		newArticle, err := service.Store(ctx, MapStoreArticleReqToCmd(article))
		if err != nil {
			c.Status(GetStatusCode(err))
			return c.JSON(ArticleErrorResponse(err))
		}

		c.Status(http.StatusCreated)
		return c.JSON(ArticleSuccessResponse(MapArticleToRes(newArticle)))
	}
}

func GetByID(service Usecase) fiber.Handler {
	return func(c *fiber.Ctx) error {
		idP, err := strconv.Atoi(c.Params("id"))
		if err != nil {
			c.Status(http.StatusBadRequest)
			return c.JSON(ArticleErrorResponse(err))
		}

		var ctx context.Context
		art, err := service.GetByID(ctx, int64(idP))
		if err != nil {
			c.Status(GetStatusCode(err))
			return c.JSON(ArticleErrorResponse(err))
		}

		return c.JSON(ArticleSuccessResponse(MapArticleToRes(art)))
	}
}

func Delete(service Usecase) fiber.Handler {
	return func(c *fiber.Ctx) error {
		idP, err := strconv.Atoi(c.Params("id"))
		if err != nil {
			c.Status(http.StatusBadRequest)
			return c.JSON(ArticleErrorResponse(err))
		}

		var ctx context.Context
		if err = service.Delete(ctx, int64(idP)); err != nil {
			c.Status(GetStatusCode(err))
			return c.JSON(ArticleErrorResponse(err))
		}

		return c.SendStatus(http.StatusAccepted)
	}
}
