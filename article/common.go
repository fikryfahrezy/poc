package article

import (
	"encoding/base64"
	"github.com/gofiber/fiber/v2"
	"github.com/pkg/errors"
	"gopkg.in/go-playground/validator.v9"
	"net/http"
	"time"
)

var (
	// ErrInternalServerError will throw if any the Internal Server Error happen
	ErrInternalServerError = errors.New("Internal Server Error")
	// ErrNotFound will throw if the requested item is not exists
	ErrNotFound = errors.New("Your requested Item is not found")
	// ErrConflict will throw if the current action already exists
	ErrConflict = errors.New("Your Item already exist")
	// ErrBadParamInput will throw if the given request-body or params is not valid
	ErrBadParamInput = errors.New("Given Param is not valid")
)

const (
	timeFormat = "2006-01-02T15:04:05.999Z07:00" // reduce precision from RFC3339Nano as date format
)

// DecodeCursor will decode cursor from user for mysql
func DecodeCursor(encodedTime string) (time.Time, error) {
	byt, err := base64.StdEncoding.DecodeString(encodedTime)
	if err != nil {
		return time.Time{}, err
	}

	timeString := string(byt)
	t, err := time.Parse(timeFormat, timeString)

	return t, err
}

// EncodeCursor will encode cursor from mysql to user
func EncodeCursor(t time.Time) string {
	timeString := t.Format(timeFormat)

	return base64.StdEncoding.EncodeToString([]byte(timeString))
}

func GetStatusCode(err error) int {
	if err == nil {
		return http.StatusOK
	}

	switch err {
	case ErrInternalServerError:
		return http.StatusInternalServerError
	case ErrNotFound:
		return http.StatusNotFound
	case ErrConflict:
		return http.StatusConflict
	default:
		return http.StatusInternalServerError
	}
}

// ArticleSuccessResponse is the singular SuccessResponse that will be passed in the response by Handler
func ArticleSuccessResponse(articleRes ArticleView) *fiber.Map {
	return &fiber.Map{
		"status": true,
		"data":   articleRes,
		"error":  nil,
	}
}

// ArticlesSuccessResponse is the list SuccessResponse that will be passed in the response by Handler
func ArticlesSuccessResponse(data []ArticleView) *fiber.Map {
	return &fiber.Map{
		"status": true,
		"data":   data,
		"error":  nil,
	}
}

// ArticleErrorResponse is the ErrorResponse that will be passed in the response by Handler
func ArticleErrorResponse(err error) *fiber.Map {
	return &fiber.Map{
		"status": false,
		"data":   "",
		"error":  err.Error(),
	}
}

func IsRequestValid(m StoreArticleCommand) (bool, error) {
	validate := validator.New()
	err := validate.Struct(m)
	if err != nil {
		return false, err
	}
	return true, nil
}

func MapAuthorToRes(author Author) AuthorView {
	return AuthorView{
		ID:        author.ID,
		Name:      author.Name,
		CreatedAt: author.CreatedAt,
		UpdatedAt: author.UpdatedAt,
	}
}

func MapStoreArticleReqToCmd(author StoreArticleRequest) StoreArticleCommand {
	return StoreArticleCommand{
		Title:   author.Title,
		Content: author.Content,
	}
}

func MapStoreArticleCmdToEntity(author StoreArticleCommand) ArticleEntity {
	return ArticleEntity{
		Title:   author.Title,
		Content: author.Content,
	}
}

func MapArticleToRes(article Article) ArticleView {
	return ArticleView{
		ID:        article.ID,
		Title:     article.Title,
		Content:   article.Content,
		Author:    MapAuthorToRes(article.Author),
		UpdatedAt: article.UpdatedAt,
		CreatedAt: article.CreatedAt,
	}
}

func MapArticlesToRes(articles []Article) []ArticleView {
	views := make([]ArticleView, len(articles))
	for i, article := range articles {
		views[i] = MapArticleToRes(article)
	}

	return views
}

func MapAuthorToEntity(author Author) AuthorEntity {
	return AuthorEntity{
		ID:        author.ID,
		Name:      author.Name,
		CreatedAt: author.CreatedAt,
		UpdatedAt: author.UpdatedAt,
	}
}

func MapAuthorEntityToAuthor(author AuthorEntity) Author {
	return Author{
		ID:        author.ID,
		Name:      author.Name,
		CreatedAt: author.CreatedAt,
		UpdatedAt: author.UpdatedAt,
	}
}

func MapArticleToEntity(article Article) ArticleEntity {
	return ArticleEntity{
		ID:        article.ID,
		Title:     article.Title,
		Content:   article.Content,
		UpdatedAt: article.UpdatedAt,
	}
}

func MapArticleEntityToArticle(article ArticleEntity) Article {
	return Article{
		ID:        article.ID,
		Title:     article.Title,
		Content:   article.Content,
		Author:    MapAuthorEntityToAuthor(article.Author),
		UpdatedAt: article.UpdatedAt,
		CreatedAt: article.CreatedAt,
	}
}

func MapArticleEntitiesToArticles(articleEntities []ArticleEntity) []Article {
	articles := make([]Article, len(articleEntities))
	for i, entity := range articleEntities {
		articles[i] = MapArticleEntityToArticle(entity)
	}
	return articles
}
