package article

import (
	"time"
)

type AuthorEntity struct {
	ID        int64
	Name      string
	CreatedAt string
	UpdatedAt string
}

// ArticleEntity ...
type ArticleEntity struct {
	ID        int64
	Title     string
	Content   string
	Author    AuthorEntity
	UpdatedAt time.Time
	CreatedAt time.Time
}
