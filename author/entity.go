package author

// Author ...
type Author struct {
	ID        int64
	Name      string
	CreatedAt string
	UpdatedAt string
}
