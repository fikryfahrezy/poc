package author

import (
	"context"
	"database/sql"
)

// Repository represent the author's repository contract
type Repository interface {
	GetByID(ctx context.Context, id int64) (author Author, err error)
}

type repository struct {
	MysqlDB *sql.DB
}

// NewRepository will create an implementation of author.Repository
func NewRepository(mysqlDb *sql.DB) Repository {
	return &repository{
		MysqlDB: mysqlDb,
	}
}

func (r *repository) GetByID(ctx context.Context, id int64) (author Author, err error) {
	query := `
		SELECT 
			id,
			name,
			created_at,
			updated_at
		FROM 
			author
		WHERE id = ?
	`

	stmt, err := r.MysqlDB.PrepareContext(ctx, query)
	if err != nil {
		return Author{}, err
	}
	row := stmt.QueryRowContext(ctx, id)

	err = row.Scan(
		&author.ID,
		&author.Name,
		&author.CreatedAt,
		&author.UpdatedAt,
	)

	return
}
